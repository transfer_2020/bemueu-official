const { default: AppSVGMountain } = require("./AppSVGMountain")

const AppHeader = () => {
    return (
        <div className="bg-header-default bg-no-repeat bg-center bg-cover text-white text-center">
            <header className="bg-black bg-opacity-70 relative">
                <div className="py-20 lg:py-40">
                    <span className="text-base">bienvenue aux amitiés</span>
                    <h1 className="font-thin text-5xl md:text-8xl sm:text-6xl">Amitiés</h1>
                    <p className="text-lg sm:text-xl">lorem ipsum dolor sit amet.</p>
                </div>
                <AppSVGMountain />
            </header>
        </div>
    )
}
 
export default AppHeader;