import { useState, useEffect } from "react";

const AppInstagram = ({sourceElement}) => {

    const [showCount, setShowCount] = useState(false)

    return (
        <>
            <div className="mx-auto my-6 relative flex-1">
                <div>
                    <a href={`https://www.instagram.com/p/${sourceElement.node.shortcode}`} target="_blank" rel="noreferrer noopener" onMouseEnter={() => setShowCount(x => !x)}>
                        <img src={sourceElement.node.thumbnail_resources[2].src} />
                    </a>
                </div>
                <div className={ (showCount ? "flex" : "hidden") + " absolute bg-black text-white w-full h-full text-center items-center justify-around top-0"} onMouseLeave={() => setShowCount(x => !x)}>
                    <div>Likes : { sourceElement.node.edge_liked_by.count }</div>
                    <div>Comment : { sourceElement.node.edge_media_to_comment.count }</div>
                </div>
            </div>
        </>
    )
}
 
export default AppInstagram;