import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";

const AppNavbarLinks = [
    { href: '/', label: 'lorem' },
    { href: '/', label: 'ipsum' },
    { href: '/', label: 'dolor' },
    { href: '/', label: 'sit' },
    { href: '/', label: 'amet' },
]

const AppNavbar = () => {

    const [showMenu, setShowMenu] = useState(false);

    return (
        <nav>
            <ul className="flex-1 flex-col xl:flex xl:flex-row items-center bg-black text-white p-3">
                <li className="flex-1">
                    <Link href="/">
                        <a>
                            <div className="flex items-center">
                                <div className="mr-3">
                                    <FontAwesomeIcon icon="star" size="2x" color="white"></FontAwesomeIcon>
                                </div>
                                <div>
                                    <h1>Amities</h1>
                                    <p>Lorem ipsum dolor sit amet</p>
                                </div>
                                <div className="xl:hidden ml-auto">
                                    <button onClick={() => setShowMenu(x => !x)}>
                                        <FontAwesomeIcon icon="bars" size="2x" color="white"></FontAwesomeIcon>
                                    </button>
                                </div>
                            </div>
                        </a>
                    </Link>
                </li>
                <li className={showMenu ? "block flex-1 items-center xl:block" : "hidden flex-1 items-center xl:block"}>
                    <ul className="flex-1 xl:flex text-center">
                        {
                            AppNavbarLinks.map(({href, label}) => (
                                <li className="flex-1 uppercase" key={`${href}${label}`}>
                                    <Link href={href}>{label}</Link>
                                </li>        
                            ))
                        }
                    </ul>
                </li>
            </ul>
        </nav>
    )
}
 
export default AppNavbar;

